const mongoose = require('mongoose');

const { database: { URI } } = require('./keys');

const connect = async () => {
  try {
    await mongoose.connect(URI, { useNewUrlParser: true });
    console.log('DB is connected');
  } catch (error) {
    console.error(error);
  }
};

module.exports = connect;
