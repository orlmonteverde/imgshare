const express = require('express');

const config = require('./server/config');
const db = require('./database');

db();

const app = config(express());

const main = async () => {
  try {
    const port = app.get('port');
    await app.listen(port);
    console.log(`Listen on port ${port}`);
  } catch (error) {
    console.error(error);
  }
};

main();
