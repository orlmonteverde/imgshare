const path = require('path');
const exphbs = require('express-handlebars');
const morgan = require('morgan');
const multer = require('multer');
const express = require('express');
const errorHandle = require('errorhandler');

const helpers = require('./helpers');
const routes = require('../routes');

module.exports = (app) => {
  // Settings.
  app.set('port', process.env.PORT || 3000);
  app.set('view', path.join(__dirname, 'views'));
  app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    partialsDir: path.join(app.get('views'), 'partials'),
    layoutsDir: path.join(app.get('views'), 'layouts'),
    extname: '.hbs',
    helpers,
  }));

  app.set('view engine', '.hbs');

  // middlewares.
  app.use(morgan('dev'));
  app.use(multer({ dest: path.join(__dirname, '../public/upload/temp') }).single('image'));
  app.use(express.urlencoded({ extended: false }));
  app.use(express.json());

  // routes.
  routes(app);

  // static files.
  app.use('/public', express.static(path.join(__dirname, '../public')));

  // errorhandlers.
  const env = app.get('env');
  if (env === 'development') {
    app.use(errorHandle);
  }

  return app;
};
