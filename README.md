# ImgShare Social Network

The minimalist social network to share images.

![MongoDB](https://img.shields.io/badge/MongoDB-v4.2.0-brightgreen.svg?logo=mongodb&longCache=true&style=flat) ![Node.js](https://img.shields.io/badge/Node.js-v10.16.3-green.svg?logo=node.js&longCache=true&style=flat)

## Getting Started

This project uses the **JavaScript** programming language with **Node.js** runtime and the **MongoDB** database engine.

### Prerequisites

[MongoDB](https://www.mongodb.com/) is required in version 3 or higher and [Node.js](https://nodejs.org/) at least in version 8.

```
Give examples
```

### Installing

The following dependencies are required:

* errorhandler 1.5.1

* express 4.17.1

* express-handlebars 3.1.0

* fs-extra 8.1.0

* md5 2.2.1

* mongoose 5.6.11

* morgan 1.9.1

* multer 1.4.2


```
npm install

```

## Deployment

Clone the repository
```
git clone git@gitlab.com:orlmonteverde/imgshare.git
```
Enter the repository folder
```
cd imgshare
```
Install dependencies
```
npm install
```
Run the program
```
npm start
```

## Built With

* [MongoDB](https://www.mongodb.com/) - The database for
modern applications
* [Node.js](https://nodejs.org/) - JavaScript runtime built on Chrome's V8 JavaScript engine
* [Express](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/orlmonteverde/imgshare/tags).

## Authors

* **Orlando Monteverde** - *Initial work* - [orlmonteverde](https://github.com/orlmonteverde)

See also the list of [contributors](https://gitlab.com/orlmonteverde/imgshare/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Fazt](https://github.com/faztweb)
